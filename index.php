<!DOCTYPE html>
<html lang="he">
<head>

    <!-- META SETTINGS -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Code in Design - Web Applications Development and Graphic Design Services, Kfir Goldfarb Freelance Full-Stack Developer, Developing Responsive Bussines & Personal Websites, Landing Pages, Web Design, UX & UI, Front-End & Back-End Developing Services, Logos Design and more.">
    <meta name="keywords" content="Code in Design, Kfir Goldfarb, programming, Website, Design, Developing, Freelance, HTML, CSS, JavaScript, Action Script, PHP, SQL, Front-End, Full-Stack, Kfir, Goldfarb, Graphic Design, Animations, Code in Design, Code,">

    <!-- TITLE -->
    <title>ברית - דף הבית</title>

    <!-- JS -->
    <script src="js/index.js"></script>
    <script src="js/topbutton.js"></script>
    <script src="js/cart.js"></script>
    <script src="js/https.js"></script>
    <script src="js/bookpage.js"></script>

    <!-- JSON -->
    <script src="data/books.js"></script>

    <!-- ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="images/icons/book-01.png">

    <!-- CSS -->
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="stylesheet" href="CSS/index.css">
    <link rel="stylesheet" href="CSS/header.css">
    <link rel="stylesheet" href="CSS/footer.css">
    <link rel="stylesheet" href="CSS/books.css">

</head>
<body onload="getDOMs();hideTopBtnOnLoad();load_cart();" onscroll="scrollFunction();">
    
    <!-- header and main -->
    <main class="main" id="main">
        <div class="fixedheader" id="fixedheader">
            <!-- header -->
            <?php require "Includes/header.html"; ?>
        </div>
        
        <div class="maincontainer" id="maincontainer">
            <div class="maintext1" id="maintext1">ברית הוצאת ספרים</div>
            <div class="maintext2" id="maintext2"><span class="hh">ארוטיקה</span> ◦ <span class="hh">רומנים</span> ◦ <span class="hh">פנטזיה</span></div>
            <input type="button" class="btn3" value="אודות">
        </div>

    </main>

    <!-- about -->
    <section class="about grey" id="about">
        <h1><img src="images/icons/about-01.svg">אודות</h1>
        <p>
            ברית הוצאת ספרים אודות יש להוסיף אודות
        </p>
        <input type="button" class="btn3" value="צור קשר">
    </section>

    <!-- index books -->
    <section class="indexbooks" id="incexbooks">
        <h1><img src="images/icons/book-01.svg">ספרים מומלצים</h1>
        <div class="books" id="books"></div>
        <script>
            let indexbooks_output = "";
            for(let i = 0; i < 4; i++) {
                indexbooks_output += '<div class="book" id="book">';
                indexbooks_output += '<img src="' + books[i].image_url + '" onclick="click_on_book(' + books[i].id + ');">';
                indexbooks_output += '<h3 class="bookname block" id="bookname">' + books[i].name + '</h3>';
                // indexbooks_output += '<span class="bookdescription block" id="bookdescription">' + books[i].description + '</span>';
                indexbooks_output += '<span class="bookprices block">';
                indexbooks_output += 'מחיר הדפסה: ';
                indexbooks_output += '<span class="bookprice" id="bookprice">' + books[i].print_price + '</span>';
                indexbooks_output += '₪';
                indexbooks_output += '</span>';
                indexbooks_output += '<span class="bookprices block">';
                indexbooks_output += 'מחיר דיגיטל: ';
                indexbooks_output += '<span class="bookpricedigital" id="bookpricedigital">' + books[i].digital_price + '</span>';
                indexbooks_output += '<span class="shekel">₪</span>';
                indexbooks_output += '</span>';
                indexbooks_output += "</div>";
            }
            document.getElementById("books").innerHTML = indexbooks_output;
        </script>
        <input class="btn4" type="button" value="ראה עוד ספרים">
    </section>

    <!-- contact -->
    <section class="contact grey" id="contact">
        <h1><img src="images/icons/contact-01.svg">יצירת קשר</h1>
        <div class="two tight">
            <div class="two">
                <div>שם פרטי:</div>
                <input type="text" class="right">
            </div>
            <div class="two">
                <div>שם משפחה:</div>
                <input type="text" class="right">
            </div>
        </div>
        <div class="two tight">
            <div class="two">
                <div>פלאפון:</div>
                <input type="tel" name="" id="" class="left">
            </div>
            <div class="two">
                <div>אימייל:</div>
                <input type="email" name="" id="" class="left">
            </div>
        </div>
        <div class="top">
            <input type="checkbox" name="" id="">
            <span>מסכים <a href="">לתנאי שימוש</a></span>
        </div>
        <input class="btn3 top" type="submit" value="שלח">
    </section>

    <section class="writers" id="writers">
        <h1>הסופרים שלנו</h1>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Beatae ipsa eum, est deserunt alias omnis distinctio culpa, modi temporibus illum voluptatibus voluptas vel aperiam. Consequuntur autem, beatae omnis corporis doloribus praesentium architecto. Nihil, alias? Deserunt pariatur, dolore voluptates sint tempore ratione debitis, in placeat natus blanditiis, ut dolorum. Officia, delectus?</p>
    </section>

    <!-- Up Button -->
    <?php include "Includes/UpBtn.html"; ?>

    <!-- footer -->
    <?php require "Includes/footer.html"; ?>

</body>
</html>